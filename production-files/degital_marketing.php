<?php include './common/header.php' ?>

<div class="body-container degital-marketing-page">
<div class="full-width-banner-area">
  <img src="./images/degital-marketing/banner-image.svg" alt="Digital Marketing">
</div>

<section class="row-1">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5">
      <img src="./images/degital-marketing/fb-marketing.svg" alt="Facebook Marketing">
    </div>
    <div class="col-md-6 align-right">
      <h2>Facebook Marketing</h2>
      <p>Market your business in the world’s no.1 and
        largest social media network - Facebook, with
        2.6billion live users worldwide (in 2020). Reach
        to overseas customers, foreign clients, or even to
        more densely populated regions of your own country.
        Our Digital Marketing Team is highly skilled and
        expertise in generating a huge number of leads for
        your business on  a daily basis through Facebook
        marketing. Thus, increasing your potential
        customers every day.</p>
    </div>
  </div>
</div>
</section>

<section class="row-2">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5 order-md-last">
      <img src="./images/degital-marketing/insta-marketing.svg" alt="Instagram Marketing">
    </div>
    <div class="col-md-6">
      <h2>Instagram Marketing</h2>
      <p>Be it the big shots or the startups, each one
          loves to be a part of the 21st century’s most
          trending and vibrant social media app - 
          Instagram. Instagram marketing helps
          your business in becoming a BRAND
          name that gets glued to your customers'
          minds. It's the perfect hub for all emerging
          startups. Even BYJU’s - India’s largest and only
          million-dollar EdTech company (as per Google
          in 2020) focuses on Instagram Marketing for
          increasing its business revenue and
          promoting its brand name into every part
          of the world.</p>
    </div>
  </div>
</div>
</section>

<section class="row-3">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5">
      <img src="./images/degital-marketing/seo.svg" alt="Search Engine Optimisation (SEO)">
    </div>
    <div class="col-md-6 align-right">
      <h2>Search Engine Optimisation (SEO)</h2>
      <p>If you want to appear on the top of search engine
        results, then our SEO experts will help you
        do that. The SEO experts expertise in
        effective keyword research analogy
        which helps your business grow by getting
        more frequent customer visits through
        enhanced SEO ranking. Thus, improving
        your lead to conversion ratio.</p>
        <ul class="list-item">
          <li><span>E-commerce SEO</span></li>
          <li><span>Local SEO</span></li>
          <li><span>Influencer Marketing</span></li>
        </ul>
    </div>
  </div>
</div>
</section>

<section class="row-4">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5 order-md-last">
      <img src="./images/degital-marketing/sem.svg" alt="Search Engine Marketing (SEM)">
    </div>
    <div class="col-md-6">
      <h2>Search Engine Marketing (SEM)</h2>
      <p>Our SEM experts help you reach the right
        customers by developing a strategy
        through attractive and engaging
        advertisements, which compels your
        customers to take immediate action.
        Thus, increasing your business
        revenue like never before.</p> 
    </div>
  </div>
</div>
</section>


<section class="row-5">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5">
      <img src="./images/degital-marketing/ppc.svg" alt="Pay Per Click (PPC) Advertisement">
    </div>
    <div class="col-md-6 align-right">
      <h2>Pay Per Click (PPC) Advertisement</h2>
      <p>PPC ads help you earn through every advertisement
        and get you the best Return on Investment.
        Our advertising experts ensure that each
        ad is targeted to your potential customers,
        thus ensuring the best results for a
        high-peak profitability.<p>
    </div>
  </div>
</div>
</section>

<section class="row-6">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5 order-md-last">
      <img src="./images/degital-marketing/google.svg" alt="Google AdWords">
    </div>
    <div class="col-md-6">
      <h2>Google AdWords</h2>
      <p>World’s best search engine provides you the
          largest and most powerful advertisement
          network - Google AdWords. This will not only
          help you reach more customers on a daily
          basis but will also create memorable
          impressions on your customers' minds.
          Google AdWords appear on web as well
          as mobile platforms, thus ensuring that
          your ads always go where your customers
          go. This increases the rate of action taken
          on per advertisement, thus increasing
          your revenue.</p> 
    </div>
  </div>
</div>
</section>


<section class="row-7">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5">
      <img src="./images/degital-marketing/email.svg" alt="Email Marketing">
    </div>
    <div class="col-md-6 align-right">
      <h2>Email Marketing</h2>
      <p>Greet your customers
        through emails that make them love your 
        business more. Our highly proficient Email-
        Marketers strategize plans which never let
        your promotion emails go to the spam. With
        proper research in understanding customer 
        behavior, we develop and create emails that
        compels your customers to take action.<p>
    </div>
  </div>
</div>
</section>


<section class="row-8">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5 order-md-last">
      <img src="./images/degital-marketing/content-marketing.svg" alt="Content Marketing">
    </div>
    <div class="col-md-6">
      <h2>Content Marketing</h2>
      <p>Catchy and thought-provoking content
        is what attracts your customers to your
        website. Our team of expert content
        marketers indulge in creating engaging
        content which are SEO-friendly and adds
        up to your business, bringing in more
        traffic to your website.</p> 
        <ul>
          <li>Blogging</li>
          <li>SEO Content Writing</li>
          <li>Content Reviewing</li>
      </ul>
    </div>
  </div>
</div>
</section>


</div>
<?php include './common/footer.php' ?>
