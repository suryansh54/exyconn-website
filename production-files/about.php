<?php include './common/header.php' ?>
<div class="body-container about-page">

  <div class="banner-area">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-6 text-md-right">
        <img src="images/about-banner-img.svg" alt="About Us">
        </div>
        <div class="col-lg-6 order-md-first mt-4 mt-md-0">
          <h2 class="banner-title">We Promise To Put All Our<br>
            Talent Into Your Project</h2>
        </div>
      </div>
    </div>
  </div>

  <section>
    <div class="container ">
      <h2 class="center-title">Principles of Work</h2>
      <div class="row principles-sec">
        <div class="col-md-4 text-container top-col">
          <img src="images/about/We Have Well-established Processes.svg" alt="">
          <h5>We Have Well-established Processes</h5>
          <p>Starting with communication and ending with solution deployment and maintenance we don’t waste a minute
            on unproductive work.</p>
        </div>

        <div class="col-md-4 text-container top-col">
          <img src="images/about/We Carry Out the Promised Things.svg" alt="">
          <h5>We Carry Out the Promised Things</h5>
          <p>XB Software has got accumulated expertise and proven level of proficiency in developing efficient
            solutions for business.</p>
        </div>

        <div class="col-md-4 text-container top-col">
          <img src="images/about/We Drive Innovations.svg" alt="">
          <h5>We Drive Innovations</h5>
          <p>We build the awesome web, creating your top-notch application using cutting-edge technologies.</p>
        </div>

        <div class="col-md-4 text-container">
          <img src="images/about/We Value Your Trust.svg" alt="">
          <h5>We Value Your Trust</h5>
          <p>XB Software aims at development process transparency, wise budget spending of a customer, and search of
            the optional solution for each project task.</p>
        </div>

        <div class="col-md-4 text-container">
          <img src="images/about/We Are at Your Side.svg" alt="">
          <h5>We Are at Your Side</h5>
          <p>We think comprehensively about a long-term solution to your business, take sequrity for scaling, use the
            latest technologies.</p>
        </div>

        <div class="col-md-4 text-container">
          <img src="images/about/Think Deeply Before We Act.svg" alt="">
          <h5>Think Deeply Before We Act</h5>
          <p>Your project needs are deeply researched by our certified business analysts to ensure that your future
            app provides value to your users.</p>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="container ">
      <div class="row">
        <div class="col-lg-3 col-sm-6 col-12">
          <h6>Our Company ></h6>
          <p>Find out all about Microsoft in India - our<br>
            company, our vision for India, and more.</p>
        </div>
        <div class="col-lg-3 col-sm-6 col-12">
          <h6>Leadership Team ></h6>
          <p>Get to know some of our people, places, and ideas.<br>
            And meet the leaders who shape our vision.
          </p>
        </div>
        <div class="col-lg-3 col-sm-6 col-12">
          <h6>What we value ></h6>
          <p>See how we give our technology and resources to<br>
            empower each person on the planet to achieve more.</p>
        </div>
        <div class="col-lg-3 col-sm-6 col-12">
          <h6>Contact Us ></h6>
          <p>Get in touch. We’re here to help.</p>
        </div>
      </div>
    </div>
  </section>


  <section>
    <div class="container ">
      <div class="row deployment-sec">
        <div class="col-12">
          <div class="deployment-title">
            <h1>Welcome to our world of high end <br> app design and development</h1>
            <p>To drive home an amazing feat. We follow a step by step course of action and stick with you till the
              very end.</p>
          </div>
          <img class="deployment-img" src="images/about/Group 5934.svg" alt="">
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="container partner-sec">
      <div class="row">
        <div class="col-12">
          <h1>Out Technology Partners</h1>
          <p>We are a partner and a collaborator not a vendor. We know how to strike a balance between business needs,
            teams and partners, and we understand the transparency it requires.</p>
        </div>
        <div class="col-md-4">
          <h6>Company Nme + Logo</h6>
        </div>
        <div class="col-md-4">
          <h6>Company Nme + Logo</h6>
        </div>
        <div class="col-md-4">
          <h6>Company Nme + Logo</h6>
        </div>

      </div>
    </div>
  </section>

  <section>
    <div class="container">
      <div class="bg-sec">
        <div class="row">
          <div class="col-md-6 left-align">
            <h5>Careers</h5>
            <p>
              Our culture is built an open communication
              togetherness and equal opportunities. Join a
              team that celebrates you daily!
            </p>
            <a href="javascript:;">Know more</a>
          </div>
          <div class="col-md-6 right-align">
            <h5>How we work</h5>
            <p>Our product development cycle ensures a
              smooth and reliable communication flow. We
              take care of you every step of the way.
            </p>
            <a href="javascript:;">Know more</a>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<?php include './common/footer.php' ?>