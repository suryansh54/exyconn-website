<?php include './common/header.php' ?>

<div class="body-container mobile-app-development-page">
<div class="banner-area">
<div class="container">
  <div class="row align-items-center">
    <div class="col-lg-5 text-md-right">
      <img src="./images/mobile-app-development/banner-image.svg" alt="Mobile App Development">
    </div>
    <div class="col-lg-6 order-md-first">
      <h2 class="banner-title">Mobile App Development</h2>
      <p class="banner-subtitle">Intuitive designs combined with compelling user
      experience are what makes our apps stand
      ahead in the digital age. Seamless experience
      driven by international standards in collaboration
      with out-of-the-box ideas is the specialty of
      Acodez's apps helping your business
      accomplish its goals.</p>
    </div>
  </div>
</div>
</div>


<section class="row-1">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5">
      <img src="./images/mobile-app-development/android.svg" alt="Android App Development">
    </div>
    <div class="col-md-6 align-right">
      <h2>Android App Development</h2>
      <p>Take your business to the next level by creating a smooth,
        user-friendly, productive Android app for your products or
        services. Our team of android app experts develop the
        most valuable apps with an undoubtedly 100% customer
        satisfaction.</p><br>
        <img src="./images/mobile-app-development/androida-app-development.svg" alt="Android App Development">
    </div>
  </div>
</div>
</section>

<section class="row-2">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5 order-md-last">
      <img src="./images/mobile-app-development/react.svg" alt="React Native App Development">
    </div>
    <div class="col-md-6">
      <h2>React Native App Development </h2>
      <p>By using React Native technology, we build
        hybrid apps for iOS and Android that bring
        your UI  to life and provide a much better
        user experience compared to apps built
        with Objective-C or Java. It can be used
        for cross-platform app development as
        well.</p>
    </div>
  </div>
</div>
</section>

<section class="row-3">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5">
      <img src="./images/mobile-app-development/iso.svg" alt="iOS App development ">
    </div>
    <div class="col-md-6 align-right">
      <h2>iOS App development </h2>
      <p>We help you expand your company in any
          consumer area, whether it be the PlayStore
          or the App Store. Our talented team of
          developers of iOS apps design visually
          pleasing, easy-to-use, and stable apps
          that will amaze your iPhone users, giving
          them supreme customer satisfaction.</p>
    </div>
  </div>
</div>
</section>

<section class="row-4">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5 order-md-last">
      <img src="./images/mobile-app-development/xamarin.svg" alt="Xamarin">
    </div>
    <div class="col-md-6">
      <h2>Xamarin</h2>
      <p>Introduced by Microsoft, our highly
        experienced Xamarin App Development
        team use Xamarin to develop cross-platform
        applications in both Android and iOS to
        deliver top-performing mobile apps for
        your business that can run on multiple
        devices.</p>
    </div>
  </div>
</div>
</section>


</div>
<?php include './common/footer.php' ?>
