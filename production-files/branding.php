<?php include './common/header.php' ?>
<div class="body-container branding-page">

  <div class="banner-area">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-6 text-md-right">
          <img src="./images/branding/banner-image.svg" alt="Branding">
        </div>
        <div class="col-lg-6 order-md-first mt-4 mt-md-0">
          <h2 class="banner-title">Branding</h2>
          <p class="banner-subtitle">A Brand identity is what makes your company
            extraordinary and adds a class to your business.
            Let your audience know about your company’s
            goals and core values with our Brand identity
            services. We help you define your company in
            a single set of innovative designs, by which
            your customers can connect to your business.</p>
        </div>
      </div>
    </div>
  </div>

  
<section class="row-1">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5">
      <img src="./images/branding/logo-maker.svg" alt="Logo Making">
    </div>
    <div class="col-md-6 align-right">
      <h2>Logo Making</h2>
      <p>A beautiful and unique logo is what connects
        your customers to your brand identity. 
        Whether you talk about Apple, Google,
        Microsoft, Facebook, or any other big brand
        in the IT and Electronics industry, you know and
        recognize them by their logos. At Exyconn,
        our highly professional brand identity
        designers focus on creating the perfect
        logo to turn your company into a brand.</p>
    </div>
  </div>
</div>
</section>

<section class="row-2">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5 order-md-last">
      <img src="./images/branding/graphic-design.svg" alt="Graphic Design">
    </div>
    <div class="col-md-6">
      <h2>Graphic Design</h2>
      <p>Giving your users a perfect intuitive visual
        experience of your website or application
        is imperative. At Exyconn, our professional
        graphic designers create designs that are
        inspired by the latest trends and user
        preferences and also suits your business
        needs. All in all, we focus on delivering
        brilliant visually attractive graphic
        designs, which make you move.</p>
      <ul>
        <li><span>Flyers, Brochures</span></li>
        <li><span>Marketing posters</span></li>
      </ul>
    </div>
  </div>
</div>
</section>

<section class="row-3">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5">
      <img src="./images/branding/explainer-video.svg" alt="Explainer videos">
    </div>
    <div class="col-md-6 align-right">
      <h2>Explainer videos</h2>
      <p>We help your business become
        more interactive by creating engaging
        and standard videos like animated videos,
        animated videos, corporate videos, etc., all
        suited to your business requirements.</p>
        <ul class="list-item-right">
          <li><span>2D animated</span></li>
          <li><span>3D animated</span></li>
          <li><span>Whiteboard animation</span></li>
        </ul>
    </div>
  </div>
</div>
</section>

<section class="row-4">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5 order-md-last">
      <img src="./images/branding/infographic.svg" alt="Infographics design">
    </div>
    <div class="col-md-6">
      <h2>Infographics design</h2>
      <p>At Exyconn, we have a team of creative
        infographics designers, who expertise in
        making your facts, information, and image
        look super classy and unforgettable.
        You can now present the perfect business
        content in front of your customers by
        making it look vibrant with our brilliant
        and eye-catching designs.</p> 
        <ul class="list-item">
          <li><span>Motion graphics</span></li>
          <li><span>Custom Infographics</span></li>
          <li><span>Video Infographics</span></li>
        </ul>
    </div>
  </div>
</div>
</section>


<section class="row-5">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-6 m-auto">
      <h2>Print design</h2>
      <img src="./images/branding/print-design.svg" alt="Print design">
      <br>
      <p>Creativity flows in our designers' hearts, and that is why we want to provide you the
        best experience of the combination of design and utility in our brilliant print design
        solutions. Make your office come alive and create an everlasting impression in front of
        your client with our print design solutions.<p>
        <ul class="list-item row col-md-8 m-auto">
          <li class="col-md-6"><span>Calendars</span></li>
          <li class="col-md-6"><span>Packaging and Labelling</span></li>
          <li class="col-md-6"><span>Hoardings</span></li>
          <li class="col-md-6"><span>Business cards</span></li>
          <li class="col-md-6"><span>Diaries</span></li>
        </ul>
    </div>
  </div>
</div>
</section>

<section class="row-6">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5 order-md-last">
      <img src="./images/branding/content-writing.svg" alt="Creative Content Writing">
    </div>
    <div class="col-md-6">
      <h2>Creative Content Writing</h2>
      <p>We use the power of words and alphabets to help
          you build a superior brand identity. We have a team
          of expert content writers who do the magic with their
          words, conveying the right message to your
          customers and just what they need to know about
          your business. We believe that creative content
          and even one beautiful statement of your business
          can create a memorable brand identity in your
          customers' minds.</p> 
          <ul class="list-item">
            <li><span>Calendars</span></li>
            <li><span>Packaging and Labelling</span></li>
            <li><span>Hoardings</span></li>
            <li><span>Business cards</span></li>
            <li><span>Diaries</span></li>
        </ul>
    </div>
  </div>
</div>
</section>

</div>
<?php include './common/footer.php' ?>
