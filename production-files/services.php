<?php include './common/header.php' ?>
<div class="body-container service-page">
  <div class="banner-area">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-6 text-md-right">
          <img src="./images/service-banner-image.svg" alt="">
        </div>
        <div class="col-lg-6 order-md-first mt-4 mt-md-0">
          <h2 class="banner-title">Services</h2>
          <p class="banner-subtitle">Omni-channel is our call and we will fuse it into all your
            business solutions, so that each of your customers are
            satisfied. Blending in the perfect essence of flexibility in our
            delivery models, with mature methodologies, innovative
            technologies,and unbeatable domain experties, Acodez IT
            Solutions will stategize a progressive workflow sculpt for your
            application developments process.</p>
          <a href="javascript:void(0)" class="banner-button">Learn More</a>
        </div>
      </div>
    </div>
  </div>

  <section>
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-5 order-md-last mb-3 mb-md-0">
          <img src="./images/2052.svg" alt="">
        </div>
        <div class="col-lg-6 col-md-6">
          <h1>UI & UX Design</h1>
          <p>UX dries UI. Our team of UX designers at Acodez have ample of years
            of experience in the core areas of user experience, which helps them
            collaborate the finer modules of usability and functionality in-line with
            business goals and end-user needs.</p>
          <h3>Our UX Design Services</h3>
          <ul>
            <li>UX Research</li>
            <li>Interaction Design</li>
            <li>Information Architechture</li>
            <li>Information Architecture</li>
            <li>Mood Boards</li>
            <li>UX Testing</li>
            <li>Wire Framing</li>
          </ul>
          <a href="javascript:void(0)" class="btn btn-primary mt-4">View More</a>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-5 mb-3 mb-md-0">
          <img src="./images/Group 2113.svg" alt="Mobile App Development">
        </div>
        <div class="col-lg-6 col-md-6">
          <h1>Mobile App Development</h1>
          <p>Intuitive designs combined with compelling user experience are what makes
            our apps stand ahead in the digital age. Seamless experience driven
            by international standards in collaboration with out-of-the-box ideas
            is the specialty of Acodez's apps helping your business accomplish its
            goals.</p>
          <h3>Our Mobile App Solutions</h3>
          <ul>
            <li>Android App Development</li>
            <li>Apple Watch App Development</li>
            <li>React Native App Development</li>
            <li>Windows Mobile App Development</li>
            <li>iPhone Development</li>
            <li>PhoneGap App Development</li>
            <li>iPad App Development</li>
            <li>Xamarin App Development</li>
          </ul>
          <a href="javascript:void(0)" class="btn btn-primary mt-4">View More</a>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-5 order-md-last mb-3 mb-md-0">
          <img src="./images/Group 3226.svg" alt="Web Development">
        </div>
        <div class="col-lg-6 col-md-6">
          <h1>Web Development</h1>
          <p>Cross-Brower and cross-device compatibility driven by mobile responsiveness
            all under one roof
            Staring from basic website designs, including CMS and online store
            building to highly complex business website apps and design solutions,
            we will customize the best of web development solutions for you.</p>
          <h3>Our Web Development Services</h3>
          <ul>
            <li>Custom Application Development</li>
            <li>Content Management System (CMS)</li>
            <li>Ecommerce Development</li>
            <li>MEAN Stack Development</li>
            <li>Social Media Apps</li>
          </ul>
          <a href="javascript:void(0)" class="btn btn-primary mt-4">View More</a>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-5  mb-3 mb-md-0">
          <img src="./images/Group 3259.svg" alt="Digital Marketing">
        </div>
        <div class="col-lg-6 col-md-6">
          <h1>Digital Marketing</h1>
          <p>Developing a long-term and reliable marketing strategy that will flourish and
            live a longer life, while ensuring your customers stay with you and tell the
            world the tales of your business's awesomeness transforming our custom-
            made business strategies into real time success.</p>
          <h3>Our Digital Marketing Services</h3>
          <ul>
            <li>Inbound Marketing</li>
            <li>Analytics Consultation</li>
            <li>SEO</li>
            <li>Online Reputation Management</li>
            <li>SEN & PPC</li>
            <li>Content Marketing</li>
            <li>Social Media Optimization</li>
            <li>Email Marketing</li>
          </ul>
          <a href="javascript:void(0)" class="btn btn-primary mt-4">View More</a>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-5 order-md-last mb-3 mb-md-0">
          <img src="./images/Group 5960.svg" alt="Branding">
        </div>
        <div class="col-lg-6 col-md-6">
          <h1>Branding</h1>
          <p>A Brand identity is what makes your company extraordinary and adds a
            class to your business. Let your audience know about your company’s
            goals and core values with our Brand identity services. We help you
            define your company in a single set of innovative designs, by which
            your customers can connect to your business.</p>
          <h3>Our Branding Services</h3>
          <ul>
            <li>Logo Making</li>
            <li>Graphic Designing</li>
            <li>Explainer videos</li>
            <li>Infographics design</li>
            <li>Print design</li>
            <li>Creative Content Writing</li>
          </ul>
          <a href="javascript:void(0)" class="btn btn-primary mt-4">View More</a>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-5  mb-3 mb-md-0">
          <img src="./images/Group 5977.svg" alt="Cloud">
        </div>
        <div class="col-lg-6 col-md-6">
          <h1>Cloud</h1>
          <p>We care about your privacy and security and that is why we have created
            the best cloud solutions for you, which helps your business run with lesser
            security hreats and better data management systems. So now you can
            spend low and gain more by hosting everything from a single Virtual
            Private Server. Also, with our Public and Private Cloud Services, you can
            manage your data flexibly without compromising your privacy. Also,
            don’t panic when you face a data disaster, our Data Disaster Management
            ensures a guaranteed and efficient Cloud Recovery system - the most
            reliable backup ever.</p>
          <h3>Our Cloud Services</h3>
          <ul>
            <li>Google Cloud</li>
            <li>Amazon Web Services</li>
            <li>Azure</li>
          </ul>
          <a href="javascript:void(0)" class="btn btn-primary mt-4">View More</a>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-5 order-md-last mb-3 mb-md-0">
          <img src="./images/Group 5960.svg" alt="Maintance">
        </div>
        <div class="col-lg-6 col-md-6">
          <h1>Maintance</h1>
          <p>A Brand identity is what makes your company extraordinary and adds a
            class to your business. Let your audience know about your company’s
            goals and core values with our Brand identity services. We help you
            define your company in a single set of innovative designs, by which
            your customers can connect to your business.</p>
          <h3>Our Branding Services</h3>
          <ul>
            <li>Logo Making</li>
            <li>Graphic Designing</li>
            <li>Explainer videos</li>
            <li>Infographics design</li>
            <li>Print design</li>
            <li>Creative Content Writing</li>
          </ul>
          <a href="javascript:void(0)" class="btn btn-primary mt-4">View More</a>
        </div>
      </div>
    </div>
  </section>

</div>
<?php include './common/footer.php' ?>
