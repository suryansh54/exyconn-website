<?php include './common/header.php' ?>
<div class="body-container interface-design-page">

  <div class="banner-area">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-6 text-md-right">
          <img src="./images/interface-design/banner-image.svg" alt="Interface Design">
        </div>
        <div class="col-lg-6 order-md-first mt-4 mt-md-0">
          <h2 class="banner-title">Interface Design</h2>
        </div>
      </div>
    </div>
  </div>

  
<section class="row-1">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5">
      <img src="./images/interface-design/web-design.svg" alt="Web Design">
    </div>
    <div class="col-md-6 align-right">
      <h2>Web Design</h2>
      <p>We boast to have one of the best web design and web 
        development teams in India. Our experts use creative
        and innovative designs, merged with the latest 
        technologies to develop the perfect layout
        of your websites. This just amazes your
        customers, giving them a user-friendly 
        and vibrant feel on your website and
        web application.</p>
    </div>
  </div>
</div>
</section>

<section class="row-2">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5 order-md-last">
      <img src="./images/interface-design/ui-design.svg" alt="UI Design">
    </div>
    <div class="col-md-6">
      <h2>UI Design</h2>
      <p>We design interactive User Interfaces that
        flow smoothly with your application’s flow
        and also represent your business in a very
        user-friendly way. Putting efforts to make
        a UI, which is easy to navigate and makes
        your customers happy, is what we focus on.</p>
    </div>
  </div>
</div>
</section>

<section class="row-3">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5">
      <img src="./images/interface-design/mobile-ui-design.svg" alt="Mobile UI Design">
    </div>
    <div class="col-md-6 align-right">
      <h2>Mobile UI Design</h2>
      <p>With our highly proficient and skilled mobile UI designers,
          we have achieved 100% user satisfaction from our clients.
          We build Mobile UI designs that flow perfectly with any
          technology, be it Android, React Native, Xamarin, or any
          other cross-platform mobile app technology. Our primary
          goal is to develop smooth and crash-free mobile apps,
          with an intuitive user interface.</p>
    </div>
  </div>
</div>
</section>

<section class="row-4">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5 order-md-last">
      <img src="./images/interface-design/lp-page-design.svg" alt="Landing Page Design">
    </div>
    <div class="col-md-6">
      <h2>Landing Page Design</h2>
      <p>When you want to create an outstanding first
        impression on your customer, you need to
        have an attractive landing page that converts
        your customers to take the sales action.
        At Exyconn, our experts in landing page
        designing delve into deep research and focus
        on the essential aspects of a conversion-based
        landing page. Right from the “Submit” button to
        the overall layout of the page, we determine where
        does your customer actually clicks, which can
        increase your profitability.</p> 
    </div>
  </div>
</div>
</section>


<section class="row-5">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5">
      <img src="./images/interface-design/blog-design.svg" alt="Blog Design">
    </div>
    <div class="col-md-6 align-right">
      <h2>Blog Design</h2>
      <p>The best way to communicate and connect with
          your customers is by making them read and
          engage with your blogs. At Exyconn, we make
          sure that we create the perfect design for your
          blogs, which attracts your customers to not only
          reading the blog but also commenting on it - the
          prime objective. Our experts design the blogs in
          such a way that it creates a visual impact on your
          readers and compels them to keep visiting your
          website to read all your beautifully crafted blogs.
          </p>
    </div>
  </div>
</div>
</section>


</div>
<?php include './common/footer.php' ?>
