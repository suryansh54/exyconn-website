<?php include './common/header.php' ?>

<div class="body-container web-development-page">
<div class="banner-area">
<div class="container">
  <div class="row align-items-center">
  <div class="col-lg-12">
      <h2 class="banner-title">Mobile App Development</h2>
    </div>
    <div class="col-lg-12">
      <img src="./images/web-development/banner-image.svg" alt="Web Development">
    </div>
  </div>
</div>
</div>


<section class="row-1">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5">
      <img src="./images/web-development/stack.svg" alt="MERN Stack Development">
    </div>
    <div class="col-md-6 align-right">
      <h2>MERN Stack Development </h2>
      <p>Our highly skilled and industrial experts use
        full-stack JavaScript solution with Mongo
        DB as the database, Express JS as backend
        framework, Redux as front-end framework,
        and Node JS as back-end runtime environment
        to create super fast and reliable web applications.
        MERN is best for creating small-scale </p><br>
        <img src="./images/web-development/logo.svg" alt="Android App Development">
    </div>
  </div>
</div>
</section>

<section class="row-2">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5 order-md-last">
      <img src="./images/web-development/strack2.svg" alt="MEAN Stack Development">
    </div>
    <div class="col-md-6">
      <h2>MEAN Stack Development</h2>
      <p>When your business needs a large-scale
        application, we recommend you to choose
        MEAN Stack development as it used by
        famous tech giants like Accenture,
        Sisense, Fiverr, etc. to develop their
        applications. Our highly skilled industry
        experts use full-stack JavaScript solution
        with Mongo DB as the database, Express
        JS as backend framework, Angular as
        front-end framework, and Node JS as
        back-end runtime environment to create
        super fast and reliable web applications
        for your business.</p>
    </div>
  </div>
</div>
</section>

<section class="row-3">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5">
      <img src="./images/web-development/e-commerce.svg" alt="E-commerce website ">
    </div>
    <div class="col-md-6 align-right">
      <h2>E-commerce website </h2>
      <p>We help you take your business online.
        Reach to global customers or expand your business in
        your region, with your e-commerce website.  
        Our e-commerce website developers deliver
        quick loading and interactive websites that help
        your customers to navigate and purchase
        easily from your site.</p><br>
        <img src="./images/web-development/woo.svg" alt="Woo">
    </div>
  </div>
</div>
</section>

<section class="row-4">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5 order-md-last">
      <img src="./images/web-development/social-media.svg" alt="Social Media apps">
    </div>
    <div class="col-md-6">
      <h2>Social Media apps</h2>
      <p>We help you grow in the social media
        business as well with our experts who
        use their creativity and innovative ideas
        to bring in new features and designs into
        your desired social media app. Starting
        from Live messaging, rewards & loyalty,
        surveys, building community pages,
        surveys, etc.</p>
        <ul class="row">
            <li class="col-md-6"><strong>Promotions</strong></li>
            <li class="col-md-6"><strong>Surveys</strong></li>
            <li class="col-md-6"><strong>Contests</strong></li>
            <li class="col-md-6"><strong>Live Messaging</strong></li>
            <li class="col-md-6"><strong>Quizzes</strong></li>
            <li class="col-md-6"><strong>Loyalty Programs</strong></li>
        </ul>
    </div>
  </div>
</div>
</section>


<section class="row-5">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5">
      <img src="./images/web-development/application.svg" alt="Custom Application Development">
    </div>
    <div class="col-md-6 align-right">
      <h2>Custom Application Development </h2>
      <p>Our expert developers specialize in developing
        fast, reliable, and dynamic customized web
        applications, suited to your business needs.
        Most of our customers feel good about our
        services as we provide them fast and
        flexible development methodology
        through Agile Workflow.</p><br>
        <p><strong>Node JS Development</strong><p>
        <p><strong>PHP Development</strong><p>
    </div>
  </div>
</div>
</section>


</div>
<?php include './common/footer.php' ?>
