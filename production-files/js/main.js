function onScroll() {
  const scroll = document.documentElement.scrollTop;
  const header = document.getElementsByClassName('header-container')[0];
  if (scroll > 150) {
    header.classList.add("scroll");
  } else {
    header.classList.remove("scroll")
  }
}
onScroll();
window.addEventListener('scroll', onScroll);




function setMenuHeight() {
  if ($(window).width() <= 768) {
    document.querySelector(".mobile-trigger").addEventListener("click", (function (e) {
      document.body.classList.toggle("mobile-open")
    }));

    $('.child-menu-wrap > a').click(function (e) {
      e.preventDefault();
      $(this).parent().addClass('child-open');
    });

    $('.dl-back-btn').click(function () {
      $(this).parent().parent().removeClass('child-open');
    });

    $('.mobile-overlay').click(function () {
      $('body').removeClass('mobile-open');
    });
  }
}
setMenuHeight();
$(window).on("resize", setMenuHeight);



$('[data-modal]').click(function () {
  $('[data-modal-content]').addClass('active-modal');
  $('.overlay').addClass('visible');
  $('body').css('overflow', 'hidden');
  $('[data-modal-close], .overlay').click(function () {
    $('[data-modal-content]').removeClass('active-modal');
    $('.overlay').removeClass('visible');
    $('body').css('overflow', 'auto');
  });
});
