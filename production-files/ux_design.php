<?php include './common/header.php' ?>

<div class="body-container ux-design-page">
<div class="banner-area">
<div class="container">
  <div class="row align-items-center">
    <div class="col-lg-5 text-md-right">
      <img src="./images/ux-design/banner-image.svg" alt="UX Design">
    </div>
    <div class="col-lg-6 order-md-first">
      <h2 class="banner-title">UI & UX Design</h2>
      <p class="banner-subtitle">UX dries UI. Our team of UX designers at Acodez have
        ample of years of experience in the core areas of user
        experience, which helps them collaborate the finer
        modules of usability and functionality in-line with 
        business goals and end-user needs.</p>
    </div>
  </div>
</div>
</div>

<section class="row-1">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5 order-md-last">
      <img src="./images/ux-design/development.svg" alt="UX Research and Development">
    </div>
    <div class="col-md-6">
      <h2>UX Research and Development</h2>
      <p>Our highly skilled UX Developers delve into
        deep research and understand customer
        behaviour before designing the UX for your
        application. We understand that UX is what
        impresses your customer and at Exyconn
        we never fail to impress with our Mobile
        UX design, UX Design for SAAS, Product UX
        design for startups, etc.</p>
    </div>
  </div>
</div>
</section>

<section class="row-2">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5">
      <img src="./images/ux-design/mood-boards.svg" alt="Mood Boards">
    </div>
    <div class="col-md-6 align-right">
      <h2>Mood Boards</h2>
      <p>We create the perfect mood boards, which look
        visually appealing and have the perfect arrangement
        of attractive images, engaging texts, and other objects
        to portray your business model, services/products
        in front of your customer in a creative way.</p>
    </div>
  </div>
</div>
</section>

<section class="row-3">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5 order-md-last">
      <img src="./images/ux-design/architecture.svg" alt="Information Architecture">
    </div>
    <div class="col-md-6">
      <h2>Information Architecture</h2>
      <p>We achieve the perfect Information Architecture with the
        help of our UX experts, who with their proficient experience,
        indulge in organizing, structuring, and labelling content in
        the most effective way. Our main objective is to help the
        users find information and complete their tasks in the
        simplest way.</p>
    </div>
  </div>
</div>
</section>

<section class="row-4">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5">
      <img src="./images/ux-design/wireframing.svg" alt="Wireframing">
    </div>
    <div class="col-md-6 align-right">
      <h2>Wireframing</h2>
      <p>Wireframes are an integral part of an interactive
        and user-friendly website design. We make
        sure that you have gone through all the
        wireframe designs before we deploy it on the
        actual working model. Only on your final
        approval, we finalize the actual design.</p>
    </div>
  </div>
</div>
</section>

<section class="row-5">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5 order-md-last">
      <img src="./images/ux-design/interaction.svg" alt="Interaction Design">
    </div>
    <div class="col-md-6">
      <h2>Interaction Design</h2>
      <p>We know that the right coupling of UI and UX makes
        your website or application design beautiful and
        seamless to use. Our main objective is to use modern
        and latest technologies to build a smooth and
        unforgettable interactive user-experience.</p>
    </div>
  </div>
</div>
</section>

<section class="row-6">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5">
      <img src="./images/ux-design/ux-testing.svg" alt="UX Testing">
    </div>
    <div class="col-md-6 align-right">
      <h2>UX Testing</h2>
      <p>You can find all your UX design needs with us.
      We research, analyse, design, conceptualize,
      and test your UX at every stage to offer you 
      a top-class UX design.</p>
    </div>
  </div>
</div>
</section>

</div>
<?php include './common/footer.php' ?>
