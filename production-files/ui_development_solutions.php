<?php include './common/header.php' ?>

<div class="body-container ui-development-solutions-page">
<div class="banner-area">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-6 text-md-right">
          <img src="./images/ui-development-solution/banner-image.svg" alt="UI Development Solutions">
        </div>
        <div class="col-lg-6 order-md-first mt-4 mt-md-0">
          <h2 class="banner-title">UI Development Solutions</h2>
        </div>
      </div>
    </div>
  </div>

<section class="row-1">
    <div class="container">
      <div class="row align-items-center justify-content-center">
      <div class="col-md-3 justify-content-center row">
          <img src="./images/ui-development-solution/react.svg" alt="React">
        </div>
        <div class="col-md-7">
          <p>Facebook uses it and so should you. Since 2015, Facebook uses React
            JS to amaze people with its interactive and visually attractive UI. Our
            experienced and excellent React JS developers use the best elements
            of React to create faster, modular and dynamic applications.</p>
        </div>
      </div>
    </div>
</section>

<section class="row-2">
    <div class="container">
      <div class="row align-items-center justify-content-center">
      <div class="col-md-3 justify-content-center row">
          <img src="./images/ui-development-solution/angular.svg" alt="Angular">
        </div>
        <div class="col-md-7">
          <p>We have one of India’s best Angular web application development
            team with highly proficient developers, who have industrial experience
            in creating amazingly fast and responsive web applications.
            They make sure your customers have a remarkable experience using
            your web applications.</p>
        </div>
      </div>
    </div>
</section>

<section class="row-3">
    <div class="container">
      <div class="row align-items-center justify-content-center">
      <div class="col-md-3 justify-content-center row">
          <img src="./images/ui-development-solution/js.svg" alt="JS">
        </div>
        <div class="col-md-7">
          <p>When you want a quality design and user experience for your web
            application, that fits into your budget, without compromising the
            user’s brilliant visual experience, then we recommend you to go for
            JavaScript. It is the perfect framework for a budget-friendly web
            application development.</p>
        </div>
      </div>
    </div>
</section>


</div>
<?php include './common/footer.php' ?>
