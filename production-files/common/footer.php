<footer>
  <div class="sub-footer">
    <div class="container">
      <ul>
        <li><a href="javascript:;"><img src="images/icon-facebook.svg" alt=""></a></li>
        <li><a href="javascript:;"><img src="images/icon-twitter.svg" alt=""></a></li>
        <li><a href="javascript:;"><img src="images/icon-insta.svg" alt=""></a></li>
        <li><a href="javascript:;"><img src="images/icon-youtube.svg" alt=""></a></li>
        <li><a href="javascript:;"><img src="images/icon-linkedin.svg" alt=""></a></li>
      </ul>
    </div>
  </div>
  <div class="main-footer">
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <div class="row">
            <div class="col-12 footer-column about-sec">
              <h5>About Us</h5>
              <p>UX Design Mobile App Development Web Development Digital Marketing Branding Interface Design</p>
            </div>
            <div class="col-12 footer-column contact">
              <h5>Contact Us</h5>
              <ul>
                <li><a href="tel:+919093416673"><span><img src="images/icon-caller.svg" alt=""></span>+91 -
                    9093416673</a></li>
                <li><a href="mailto:bibaswanjana@gmail.com"><span><img src="images/icon-mail.svg" alt=""></span>bibaswanjana@gmail.com</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-5">
          <div class="row">
            <div class="col-md-6 footer-column">
              <h5>Services</h5>
              <ul class="footer-nav">
                <li><a href="ux_design.php">UX Design</a></li>
                <li><a href="mobile_app_development.php">Mobile App Development</a></li>
                <li><a href="web_development.php">Web Development</a></li>
                <li><a href="degital_marketing.php">Digital Marketing</a></li>
                <li><a href="branding.php">Branding</a></li>
                <li><a href="interface_design.php">Interface Design</a></li>
                <li><a href="emerging_technology.php">Emerging Technology</a></li>
                <li><a href="ui_development_solutions.php">UI Development Solutions</a></li>
                <li><a href="cloud.php">Cloud</a></li>
                <li><a href="maintainance.php">Maintenance</a></li>
              </ul>
            </div>
            <div class="col-md-6 footer-column">
              <h5>Marketplace</h5>
              <ul class="footer-nav">
                <li><a href="javascript:;">Website Template</a></li>
                <li><a href="javascript:;">Email Template</a></li>
                <li><a href="javascript:;">Pricing</a></li>
                <li><a href="javascript:;">Server</a></li>
                <li><a href="javascript:;">Utilities</a></li>
                <li><a href="javascript:;">Tool</a></li>
                <li><a href="javascript:;">Extension</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-3 footer-column">
          <form action="" class="subscribe-form">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Enter your email">
            </div>
            <div class="form-group">
              <button class="btn btn-submit">Subscribe</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</footer>
<script type="text/javascript" src="js/main.js"></script>
</body>
</html>
