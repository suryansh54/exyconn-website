<!DOCTYPE html>
<html>
  <head>
    <title>Exyconn</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  </head>
  <body>
<div class="overlay"></div>
<header class="header-container">
  <div class="top-pane">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-5">
          <ul>
            <li><a data-modal="country" href="javascript:;" class="country-language"><img class="flag-icon" style=" width: 21px;" src="images/country-icons/india.svg" alt=""> India</a></li>
            <li><a href="javascript:;" class="contact-sales"><i class="fas fa-user-tie"></i> Contact Sales</a></li>
          </ul>
        </div>
        <div class="col-7">
          <ul class="right">
            <li>
              <div class="search-wrapper">
                <input placeholder="Search..." type="text" class="search-field">
                <button class="search-btn"><i class="fas fa-search"></i></button>
              </div>
            </li>
            <li><a href="javascript:;">Customer Support</a></li>
            <li><a href="javascript:;">Sign In</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="main-header">
    <div class="container">
      <div class="logo">
        <a href="/">
          <img src="images/logo.svg" alt="Exyconn logo">
        </a>
      </div>
      <div class="mobile-trigger">
        <i class="fas fa-bars"></i>
      </div>
      <nav class="main-nav">
        <ul class="right">
          <li class="active child-menu-wrap"><a href="index.php">Home
              <svg class="icon icon--arrow-bottom" viewBox="0 0 12 8" role="presentation">
                <path stroke="currentColor" stroke-width="2" d="M10 2L6 6 2 2" fill="none" stroke-linecap="square">
                </path>
              </svg>
              <svg class="icon icon--arrow-right" viewBox="0 0 8 12" role="presentation">
                <path stroke="currentColor" stroke-width="2" d="M2 2l4 4-4 4" fill="none" stroke-linecap="square">
                </path>
              </svg>
            </a>
            <ul class="child-menu label-2">
              <li class="dl-back-btn"><svg class="icon icon--arrow-left" viewBox="0 0 8 12" role="presentation">
                  <path stroke="currentColor" stroke-width="2" d="M6 10L2 6l4-4" fill="none" stroke-linecap="square">
                  </path>
                </svg> Back</li>
              <li class="active child-menu-wrap"><a href="javascript:;">Demo menu 2<svg class="icon icon--arrow-right" viewBox="0 0 8 12" role="presentation">
                    <path stroke="currentColor" stroke-width="2" d="M2 2l4 4-4 4" fill="none" stroke-linecap="square">
                    </path>
                  </svg></a>

                <ul class="child-menu label-3">
                  <li class="dl-back-btn"><svg class="icon icon--arrow-left" viewBox="0 0 8 12" role="presentation">
                      <path stroke="currentColor" stroke-width="2" d="M6 10L2 6l4-4" fill="none" stroke-linecap="square"></path>
                    </svg> Back</li>
                  <li><a href="javascript:;">Demo menu 3</a></li>
                  <li><a href="javascript:;">Demo menu 3</a></li>
                  <li><a href="javascript:;">Demo menu 3</a></li>
                  <li><a href="javascript:;">Demo menu 3</a></li>
                  <li><a href="javascript:;">Demo menu 3</a></li>
                </ul>
              </li>
              <li><a href="javascript:;">Demo menu 2</a></li>
              <li><a href="javascript:;">Demo menu 2</a></li>
              <li><a href="javascript:;">Demo menu 2</a></li>
              <li><a href="javascript:;">Demo menu 2</a></li>
            </ul>
          </li>
          <li class="child-menu-wrap"><a href="about.php">About us
              <svg class="icon icon--arrow-bottom" viewBox="0 0 12 8" role="presentation">
                <path stroke="currentColor" stroke-width="2" d="M10 2L6 6 2 2" fill="none" stroke-linecap="square">
                </path>
              </svg>
              <svg class="icon icon--arrow-right" viewBox="0 0 8 12" role="presentation">
                <path stroke="currentColor" stroke-width="2" d="M2 2l4 4-4 4" fill="none" stroke-linecap="square">
                </path>
              </svg>
            </a>
            <ul class="child-menu label-2">
              <li class="dl-back-btn"><svg class="icon icon--arrow-left" viewBox="0 0 8 12" role="presentation">
                  <path stroke="currentColor" stroke-width="2" d="M6 10L2 6l4-4" fill="none" stroke-linecap="square"></path>
                </svg> Back</li>
              <li><a href="how-we-work.php">How we Work</a></li>
              <li><a href="javascript:;">Demo menu 3</a></li>
              <li><a href="javascript:;">Demo menu 3</a></li>
              <li><a href="javascript:;">Demo menu 3</a></li>
              <li><a href="javascript:;">Demo menu 3</a></li>
            </ul>
          </li>
          <li><a href="javascript:;">Blogs</a></li>
          <li><a href="services.php">Services</a></li>
          <li><a href="javascript:;">Team</a></li>
          <li><a href="contact.php">Contact</a></li>
        </ul>

        <div class="mobile-bottom-content">
          <div class="row align-items-center">
            <div class="col-5 lang-link">
              <ul>
                <li><a href="javascript:;"><img src="images/language icon.svg" alt=""> India</a></li>
                <li><a href="javascript:;"><img src="images/contact-sales.svg" alt=""> Contact Sales</a></li>
              </ul>
            </div>
            <div class="col-7 user-link">
              <ul class="right">
                <li><a href="javascript:;">Customer Support</a></li>
                <li><a href="javascript:;">Sign In</a></li>
              </ul>
            </div>
          </div>
        </div>
      </nav>
    </div>
  </div>
  <div class="mobile-overlay"></div>
</header>

<div data-modal-content="country">
  <div class="cm-modal-header">
    <div class="cm-modal-header-title">
      <h3>Select Country</h3>
    </div>
    <a class="cm-modal-header-close" data-modal-close>
      <i class="fas fa-times"></i>
    </a>
  </div>
  <div class="cm-modal-body">
    Country List
  </div>
</div>
