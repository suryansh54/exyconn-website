<?php include './common/header.php' ?>

<div class="body-container emerging-technology-page">
<div class="banner-area">
<div class="container">
  <div class="row align-items-center">
  <div class="col-lg-12">
      <h2 class="banner-title">Banner Area</h2>
    </div>
  </div>
</div>
</div>


<section class="row-1">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5">
      <img src="./images/emerging-technology/chatbot-development.svg" alt="Chatbot Development">
    </div>
    <div class="col-md-6 align-right">
      <h2>Chatbot Development</h2>
      <p>Make your website, web and mobile applications
          more interactive with a virtual chat assistant -
          Chatbot. Now your customers will have their
          queries answered within seconds, be it regarding
          your products, services or policies,the chatbot will
          always be there to help them with the generic
          queries. Our Artificial Intelligence experts put
          in their best experience to create user-friendly
          and efficient chatbots.</p>
    </div>
  </div>
</div>
</section>

<section class="row-2">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5 order-md-last">
      <img src="./images/emerging-technology/iot-applications.svg" alt="IoT Applications">
    </div>
    <div class="col-md-6">
      <h2>IoT Applications</h2>
      <p>With the growing IoT application market, our IoT
        experts provide the best-in-class IoT applications,
        built with reliable technologies and excellent
        features, that suits your business needs and
        provide the right service to your customers.</p>
    </div>
  </div>
</div>
</section>

<section class="row-3">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5">
      <img src="./images/emerging-technology/wearable-app-development.svg" alt="Wearable App Development">
    </div>
    <div class="col-md-6 align-right">
      <h2>Wearable App Development</h2>
      <p>At Exyconn, our wearable application developers
        are highly experienced in simplifying complex
        app developments in such a way that it surprises
        the user with its smooth usability and functionality.
        So now we help your customers carry
        user-friendly technology wherever they go.</p>
    </div>
  </div>
</div>
</section>

<section class="row-4">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5 order-md-last">
      <img src="./images/emerging-technology/augmented-reality-apps.svg" alt="Augmented Reality Apps">
    </div>
    <div class="col-md-6">
      <h2>Augmented Reality Apps</h2>
      <p>If you want your customers to experience the
        power of real technology, then amaze them
        with the visually appealing and brilliant
        functionality of our Augmented Reality
        Apps (AR Apps), suited to your business
        requirements. Be it location based-app,
        or any other app, we assure you the best
        feeling of augmented reality.</p>
    </div>
  </div>
</div>
</section>


<section class="row-5">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5">
      <img src="./images/emerging-technology/big-data-services.svg" alt="Big Data Services">
    </div>
    <div class="col-md-6 align-right">
      <h2>Big Data Services</h2>
      <p>When your business grows, your data grows.
        This is why you need our Big Data services
        from our Big Data experts who understand your
        business and provide you customised Big Data
        Solutions in accordance to your business size.</p>
    </div>
  </div>
</div>
</section>


<section class="row-6">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-6 m-auto">
    <img src="./images/emerging-technology/cloud-computing.svg" alt="Big Data Services">
    <br>
      <p>We know you have a lot of data, but is it safe and secure? At Exyconn, we help you preserve
        the privacy of your data by keeping it safe with our reliable cloud-based solutions.
        Now you don’t have to worry about lost data,
        we keep it all stored with our cloudcomputing experts.
        Be it your primary data or excess data, we make sure you have it all stored at one place
        at an affordable cost.</p>
    </div>
  </div>
</div>
</section>

</div>
<?php include './common/footer.php' ?>
