<?php include './common/header.php' ?>
<div class="body-container cloud-page">
<div class="banner-area">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-6 text-md-right">
          <img src="./images/cloud/banner-image.svg" alt="Cloud">
        </div>
        <div class="col-lg-6 order-md-first mt-4 mt-md-0">
          <h2 class="banner-title">Cloud</h2>
          <p class="banner-subtitle">We care about your privacy and security and that is why we have created the
              best cloud solutions for you, which helps your business run with lesser security
              hreats and better data management systems. So now you can spend low and
              gain more by hosting everything from a single Virtual Private Server. Also, with
              our Public and Private Cloud Services, you can manage your data flexibly
              without compromising your privacy. Also, don’t panic when you face a data
              disaster, our Data Disaster Management ensures a guaranteed and efficient
              Cloud Recovery system - the most reliable backup ever. </p>
        </div>
      </div>
    </div>
  </div>


<section class="row-1">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-xl-7 m-auto">
        <img class="cm-brand-logo" src="./images/cloud/google_cloud.svg" alt="Google Cloud">
        <p>With Google Cloud, we provide you one of the best cloud services, where you can get an
          execution environment without connecting to any server.
          Some of its best features are:-</p>
          <ul>
            <li>Serverless functionality and execution.</li>
            <li>It scales up automatically based on the data load.</li>
            <li>The best part is that it has a built-in security system, which ensures proper safety of your data.</li>
            <li>It can be used for hybrid and multi-cloud scenarios.</li>
          </ul>
      </div>
    </div>
  </div>
</section>

<section class="row-2">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-xl-7 m-auto">
        <img class="cm-brand-logo" src="./images/cloud/amazon.svg" alt="Amazon">
        <p>Amazon Web Services or AWS is one of the leading cloud services from Amazon. It is used by top
          companies in the world and we use AWS to give you a multipurpose workload platform, where you
          can almost do anything, starting from game development, data processing to many more key
          development areas of your business processes. We prefer AWS for some of the key features, 
          which are as follows:-</p>
          <ul>
            <li>It has a Virtual Private Clouds (VPCs), which means you can have your private test or virtual cloud.</li>
            <li>It has a built-in data encryption feature.</li>
            <li>AWS has a Trusted Advisor which helps you achieve more by spending less and
                optimises the performance of your system too. Also, it takes care incase of
                security gaps.</li>
          </ul>
      </div>
    </div>
  </div>
</section>

<section class="row-3">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-xl-7 m-auto">
        <img class="cm-brand-logo" src="./images/cloud/microsoft.svg" alt="Microsoft">
        <p>Azure Cloud Service system, from Microsoft is one of the trusted cloud services in the market,
            which is reputed to be a reliable cloud service for the MNCs. Microsoft Azure is best known for
            its following features:-</p>
          <ul>
            <li>It is highly scalable as per your demand.</li>
            <li>It has a reliable customer support and is also cost-effective.</li>
            <li>It allows you to work on multi-cloud systems or hybrid platforms.</li>
            <li>You can trust Azure with its simple, secure and reliable data storing capabilities.</li>
          </ul>
      </div>
    </div>
  </div>
</section>




</div>
<?php include './common/footer.php' ?>
