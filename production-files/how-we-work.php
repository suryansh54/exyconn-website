<?php include './common/header.php' ?>
<div class="body-container how-we-work-page">
  <div class="banner-area">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-6 text-md-right">
          <img src="images/How-we-work/banner-image.svg" alt="">
        </div>
        <div class="col-lg-6 order-md-first mt-4 mt-md-0">
          <h2>How we Work</h2>
          <p>Drew Guff, managing director and founding partner of
            Singular Guff & Company :
            "If you have a mission impossible project, send it to Belarus"</p>
          <p>Starting from the moment of your request and throughout
            the project we direct all energy and efforts to be your
            reliable partner.</p>
          <a href="javascript:void(0)" class="banner-button">Learn More</a>
        </div>
      </div>
    </div>
  </div>

  <section>
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-12 center-heading">
          <h3>Process of Collaboration</h3>
        </div>
        <div class="col-md-5 order-md-last mb-3 mb-md-0">
          <img src="images/How-we-work/Process-of-collaboration.svg" alt="Process of Collaboration">
        </div>
        <div class="col-lg-6 col-md-6">
          <p>The Client comes to XB Software with some business needs or idea, often not fully formed and clear. We
            offer options of collaboration and services depending
            on the request. Understanding that your idea needs protection, we guarantee your information privacy from
            a third party and sign the NDA.
            In order to best organize our future collaboration, we offer three different types of contacts.
          </p>
          <ul>
            <li><strong>The Fixed Price</strong></li>
            <li><strong>Time & Materials</strong>></li>
            <li><strong>BFS (Budget with Float Scope)</strong></li>
          </ul>
          <p>The contract type option is chosen depending on the amount of work, your wishes,
            the depth, and uniqueness of understanding the requirements for functionality,
            the availability of documentation. The start of work begins with the signing of
            an agreement by email. Work is usually divided into 2-4 week stages. We perform
            the stages, and the client pays only for accepted results. After completion
            of the work, the entire warranty period, the bug-fixing is for our own expense.</p>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="development-process">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-md-12 center-heading">
            <h3>Full-Cycle Development Process</h3>
            <p><strong>The work on the project is divided into the following phases</strong></p>
          </div>


          <div class="row-cols-lg-5 row">
            <div class="col-sm-6">
              <img src="images/How-we-work/Discovery-Phase.svg" alt="Process of Collaboration">
              <h5>Discovery Phase</h5>
              <p>requirement gathering and analysis</p>
            </div>

            <div class="col-sm-6">
              <img src="images/How-we-work/Development-Phase.svg" alt="Process of Collaboration">
              <h5>Development Phase </h5>
              <p>design, front-end and back-end development</p>
            </div>

            <div class="col-sm-6">
              <img src="images/How-we-work/Testing.svg" alt="Process of Collaboration">
              <h5>Testing</h5>
              <p>full lifecycle testing starting with requirements stages up to final testing at the release and
                deployment stages</p>
              </h5>
            </div>

            <div class="col-sm-6">
              <img src="images/How-we-work/Deployment.svg" alt="Process of Collaboration">
              <h5>Deployment</h5>
              <p>deployment of the system on the customer's environment</p>
              </h5>
              </h5>
            </div>

            <div class="col-sm-6">
              <img src="images/How-we-work/Maintenance.svg" alt="Process of Collaboration">
              <h5>Maintenance</h5>
              <p>quality guarantee period and customer support</p>
            </div>
          </div>

        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-3  mb-3 mb-md-0">
          <img src="images/How-we-work/computer.svg" alt="Computer">
        </div>
        <div class="col-lg-9 col-md-6">
          <p>The discovery phase is performed either remotely or at the customer's side to fully immerse
            the team into the process and find out all requirements for the future system. During thephase:</p>
          <p>The Scope of Work (SOW) is developed. The entire amount of work is divided into milestones.
            The development plan is created.<br>
            In accordance with the plan, specification are written for the milestones.</p>
          <p>All these steps are approved by the customer.</p>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="container">
      <div class="row  align-items-center">
        <div class="col-md-3 order-md-last mb-3 mb-md-0">
          <img src="images/How-we-work/Discovery.svg" alt="Computer">
        </div>
        <div class="col-lg-9 col-md-6">
          <h6>Discovery Phase</h6>
          <p>At the discovery phase, the team of a business analyst (BA), designer and project manager (PM) work
            simultaneously to maximize the effectiveness
            of the requirements collection process.</p>
          <p>A PM is responsible for organizing the development process, including the daily
            meetings, the timing control, the outcome of each stage, the budget. A PM solves
            issues that arise on the project.</p>
          <p>A BA is a specialist who analyses business processes and creates optimal solutions,
            collects requirements and forms them properly, solves the logical problems of the
            system, finds solutions with the team. This is a person who will be fully involved in
            the project.
            <p>A designer is responsible for developing the visuals of the future system.
              <p>After the Scope is developed, the whole work volume is divided into logical parts
                (milestones) time-frames during which separate modules (parts) of the whole
                solution are being developed.</p>
              <p>This development plan with milestones is approved by the customer. For each milestone, a specification is
                worked out.</p>
            </p>
        </div>
      </div>
    </div>
  </section>


  <section>
    <div class="container">
      <div class="row  align-items-center">
        <div class="col-md-3  mb-3 mb-md-0">
          <img src="images/How-we-work/Development.svg" alt="Computer">
        </div>
        <div class="col-lg-9 col-md-6">
          <h6>Development Phase</h6>
          <p>Based on specification and pages design, development team starts the work.
            The development is divided into short sprints. The team masters front-end and
            back-end technologies.</p>
          <p>The results of each development phase go through a full testing cycle. All bugs are
            fixed as soon as possible (ASAP). The quality system analysis includes checking the
            correct functioning of the software, the absence of technical and logical errors,
            usability testing, load testing, etc.</p>
          <p>Simultaneously, the team continues to write a specification for the next milestone.
            The development goes on taking into account the results of the previous stage,
            new ideas, and desires. This is how all milestones are implemented.</p>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h6>Deployment Phase</h6>
          <p>When all modules are ready, a team of a PM, BA, Developer, DevOps specialist, and a Team Lead work on the
            deployment of the system on the
            customer’s environment. The team configure, customize, perform tests, teach users and achieve stable system
            operation.</p>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h6>Maintenance Phase</h6>
          <p>For 2 months from the moment of the deployment, the customer has got guaranteed 24/7 support.</p>
          <p>For the next 3 months, the customer is guaranteed the support for the most efficient and smooth operation with the system.</p>
        </div>
      </div>
    </div>
  </section>
</div>
<?php include './common/footer.php' ?>
