<?php include './common/header.php' ?>
<div class="body-container">

<div class="banner-area home-banner">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-6 text-md-right">
      <img src="images/art-banner.svg" alt="Services">
      </div>
      <div class="col-lg-6 order-md-first mt-4 mt-md-0">
        <h2>Grow Your Business with Our Services</h2>
        <p>Our primary aim is to help you grow your business in a dynamic way. Creating designs that make your
          customers admire your website or mobile apps, is what we try to accomplish. With our in-depth analysis of
          customer behavior, we create user-friendly UI/UX that adapts to your customers' behaviors.</p>
        <p>Therefore, extending the right service to your customers. Since our inception, we have always focused on
          the concept of “Innovation before Implementation”, which makes sure that there is no duplicity or
          blandness in technology and design. Hence, delivering unique and admirable projects to every valuable
          client.</p>
          <a href="javascript:void(0)" class="banner-button">Learn More</a>
      </div>
    </div>
  </div>
</div>

<section>
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2>Expert in IT Development Services</h2>
      </div>
      <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
        <div class="card">
          <img class="card-img-top" src="images/csd.png" alt="">
          <div class="card-body">
            <h4 class="card-title">Custom Software Development</h4>
            <p>We help you develop your customised software for your business, which renders the right services to
              your customers. Software that interacts with the user and fulfils all his/her requirements.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
        <div class="card">
          <img class="card-img-top" src="images/mad.png" alt="">
          <div class="card-body">
            <h4 class="card-title">Mobile Application Development</h4>
            <p>Get some intuitive designs for your mobile apps and render your product/ services on Play Store and
              App Store. Apps with an interactive UI/UX that blends perfectly with the latest technology and gives
              your users a fast smooth experience.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
        <div class="card">
          <img class="card-img-top" src="images/qast.png" alt="">
          <div class="card-body">
            <h4 class="card-title">QA and Software Testing</h4>
            <p>Every Car needs a mechanic and so does your software. Don't worry if you face glitches or get bugs,
              we'll always be at your service to offer the most efficient service to get your business back on
              track.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6">
        <div class="card">
          <img class="card-img-top" src="images/itsa.png" alt="">
          <div class="card-body">
            <h4 class="card-title">IT Staff Augmentation</h4>
            <p>Out IT Staff Augmentation service ensure no compromise with the quality and quantity of your projects
              as we deploy our skilled experts according to your needs. we have deployed a system that ensures the
              total monitoring and management of your project as soon as it is outsourced to our experts. This
              ensures project completion on time and with maximum efficiency.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section>
  <div class="container">
    <div class="row align-items-center">
      <div class="col-md-6 offset-lg-1 order-md-last mb-3 mb-md-0">
        <img src="images/how-we-work.svg" width="700" alt="">
      </div>
      <div class="col-lg-5 col-md-6">
        <h2>How we Work</h2>
        <p>Drew Guff, managing director and founding partner of Singular Guff & Company : <br>"If you have a
          mission impossible project, send it to Belarus"</p>
        <p>Starting from the moment of your request and throughout the project we direct all energy and efforts to
          be your reliable partner.</p>
        <a href="javascript:void(0)" class="btn btn-primary mt-4">Let's Collabarate</a>
      </div>
    </div>
  </div>
</section>
<section>
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2>Our Services</h2>
        <p>Omni-channel is our call and we will fuse it into all your business solutions, so that each of your
          customers are satisfied. Blending in the perfect essence of flexibility in our delivery models, with
          mature methodologies, innovative technologies,and unbeatable domain experties, Acodez IT Solutions will
          stategize a progressive workflow sculpt for your application developments process.</p>
      </div>
      <div class="col-lg-3 col-sm-6 col-12">
        <div class="service-box">
          <img src="images/ui-ux.svg" height="175" alt="">
          <h5 class="mt-4">UI / UX Design</h5>
        </div>
      </div>
      <div class="col-lg-3 col-sm-6 col-12">
        <div class="service-box">
          <img src="images/web-development.svg" height="175" alt="">
          <h5 class="mt-4">Web Development</h5>
        </div>
      </div>
      <div class="col-lg-3 col-sm-6 col-12">
        <div class="service-box">
          <img src="images/mobile-app-development.svg" height="175" alt="">
          <h5 class="mt-4">Mobile App Development</h5>
        </div>
      </div>
      <div class="col-lg-3 col-sm-6 col-12">
        <div class="service-box">
          <img src="images/digital-marketing.svg" height="175" alt="">
          <h5 class="mt-4">Digital Marketing</h5>
        </div>
      </div>
    </div>
  </div>
</section>
<section>
  <div class="container">
    <div class="row align-items-center">
      <div class="col-md-4 text-center order-md-last mb-3 mb-md-0">
        <img src="images/key-solution.svg" alt="">
      </div>
      <div class="col-md-8">
        <h2>Key Solutions</h2>
        <p>Key Solutions Created by our software development services company include GPS fleet management system,
          real estate software, data visualization software, ERP solutions and more. Work with us and you get access
          to one of the most efficient and dedicated enterprise software development companies around. Key Solutions
          Created by our software development services company include GPS fleet management system, real estate
          software, data visualization software, ERP solutions and more. Work with us and you get access to one of
          the most efficient and dedicated enterprise software development companies around.</p>
      </div>
    </div>
  </div>
</section>
<section>
  <div class="container">
    <div class="row justify-content-center why-choose">
      <div class="col-12">
        <h2 class="center-text">Why Choose Us</h2>
      </div>
      <div class="col-10">
        <ul class="quality-list row">
          <li class="col-lg-3 col-sm-6 col-12">
            <span class="number">154%</span>
            <span class="quality">Avg. Company Growth</span>
          </li>
          <li class="col-lg-3 col-sm-6 col-12">
            <span class="number">70%</span>
            <span class="quality">Client Retention Rate</span>
          </li>
          <li class="col-lg-3 col-sm-6 col-12">
            <span class="number">175</span>
            <span class="quality">Complete Projects</span>
          </li>
          <li class="col-lg-3 col-sm-6 col-12">
            <span class="number">12</span>
            <span class="quality">Year of Experience</span>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-12">
        <h2 class="center-text">Key Technologies We Use</h2>
      </div>
      <div class="col-12">
        <div class="key-modules-wrapper">
          <div class="key-module row">
            <div class="col-lg-2 type-title align-center">
              <h5>Frontend</h5>
            </div>

            <div class="col-lg-2 radio-list align-center">
              <ul>
                <li>
                  <div class="radio-wrapper">
                    <input type="radio" name="frontend" id="" checked>
                    <span>React.js</span>
                  </div>
                </li>
                <li>
                  <div class="radio-wrapper">
                    <input type="radio" name="frontend" id="">
                    <span>Marionette.js</span>
                  </div>
                </li>
                <li>
                  <div class="radio-wrapper">
                    <input type="radio" name="frontend" id="">
                    <span>Webix</span>
                  </div>
                </li>
              </ul>
            </div>

            <div class="col-lg-8 dec-wrap align-center">
              <div>
                <div class="key-title"><img src="images/react-logo.svg" width="60" alt=""><span>React.js</span>
                </div>
                <div class="key-description">
                  <p>Our web and software product development company offers a full range of web development
                    services
                    and has competence in custom application development using React.js technology. The company’s
                    portfolio includes a range of projects with React.js, such as web-based video conferencing apps,
                    stock trading and commerce solutions, along with other IT solutions for clients from the USA,
                    Canada, EU and UK.</p>
                </div>
              </div>
            </div>
          </div>

          <hr>

          <div class="key-module row">
            <div class="col-lg-2 type-title align-center">
              <h5>Backend</h5>
            </div>

            <div class="col-lg-2 radio-list align-center">
              <ul>
                <li>
                  <div class="radio-wrapper">
                    <input type="radio" name="frontend" id="" checked>
                    <span>React.js</span>
                  </div>
                </li>
                <li>
                  <div class="radio-wrapper">
                    <input type="radio" name="frontend" id="">
                    <span>Marionette.js</span>
                  </div>
                </li>
                <li>
                  <div class="radio-wrapper">
                    <input type="radio" name="frontend" id="">
                    <span>Webix</span>
                  </div>
                </li>
              </ul>
            </div>

            <div class="col-lg-8 dec-wrap align-center">
              <div>
                <div class="key-title"><img src="images/react-logo.svg" width="60" alt=""><span>React.js</span>
                </div>
                <div class="key-description">
                  <p>Our web and software product development company offers a full range of web development
                    services
                    and has competence in custom application development using React.js technology. The company’s
                    portfolio includes a range of projects with React.js, such as web-based video conferencing apps,
                    stock trading and commerce solutions, along with other IT solutions for clients from the USA,
                    Canada, EU and UK.</p>
                </div>
              </div>
            </div>
          </div>


        </div>
      </div>

      <div class="consultation-btn col-12">
        <div class="col-12 mt-5 mb-5">
          <h3>Need help with your project? Feel free to contact our customer care manager.</h3>
          <a href="javascript:void(0)" class="btn btn-primary">I'D LIKE TO GET A FREE CONSULTATION</a>
        </div>
      </div>

    </div>
  </div>

  <div class="comment-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <h3>Recent Works</h3>
          <blockquote>
            <p>I'm speechless!! It's brilliant! Thank You for the great job.</p>
            <p class="byline"><span class="byline-name">Ade Okusanya</span><small>Director, Hermes Global Capital
                LLC,
                UK</small></p>
          </blockquote>
        </div>
        <div class="col-md-4">
          <h3>Read Our Blog</h3>
          <blockquote>
            <p>I'm speechless!! It's brilliant! Thank You for the great job.</p>
            <p class="byline"><span class="byline-name">Ade Okusanya</span><small>Director, Hermes Global Capital
                LLC,
                UK</small></p>
          </blockquote>
        </div>
        <div class="col-md-4">
          <h3>Happy Clients</h3>
          <blockquote>
            <p>I'm speechless!! It's brilliant! Thank You for the great job.</p>
            <p class="byline"><span class="byline-name">Ade Okusanya</span><small>Director, Hermes Global Capital
                LLC,
                UK</small></p>
          </blockquote>
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="whats-new-module">
    <div class="container">
      <div class="row">
        <div class="col-md-3 align-center">
          <h1>What's New</h1>
        </div>
        <div class="col-md-6 align-center text">
          <h6>Signed contract for a 5000+ hours Web Application for an investor firm in Saudi Arabia</h6>
        </div>
        <div class="col-md-3 align-center">
          <img src="images/Group 3853.svg" alt="">
        </div>
      </div>
    </div>
  </div>
</section>
</div>
<?php include './common/footer.php' ?>
