<?php include './common/header.php' ?>
<div class="body-container contact-page">

  <div class="banner-area">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-6 text-md-right">
          <img src="./images/contact/banner-bg.svg" alt="">
        </div>
        <div class="col-lg-6 order-md-first mt-4 mt-md-0">
          <h2 class="banner-title">Get In Touch</h2>
          <p class="banner-subtitle">
          <span>India : +91 - 9093416673</span><br>
          <span>Email : info@exyconn.com</span><br>
          <span>Career : career@exyconn.com</span>
          </p>
        </div>
      </div>
    </div>
  </div>
  
<section>
<div class="contact-from-wrapper">
  <div class="container">
    <div class="row">

      <div class="contact-from col-md-12">
          <form class="ex-form col-md-12">
            <div class="from-title col-md-12">
              <h3>We'd love to hear from you!</h3>
              <p>Brief us your requirements below, and let's connect.</p>
           </div>

           <div class="col-lg-6">
              <div class="ex-input-wrap">
                <label>Your Name <span class="ex-required ">*</span></label>
                <input class="ex-input" type="text" placeholder="Enter your name here">
              </div>

              <div class="ex-input-wrap">
                <label>Email <span class="ex-required ">*</span></label>
                <input class="ex-input" type="email" placeholder="Enter your email here">
              </div>

              <div class="ex-input-wrap">
                <label>Contact Number</label>
                <input class="ex-input" type="number" placeholder="Enter your number">
              </div>
            </div>

            <div class="col-lg-6">
              <div class="ex-input-wrap ex-textarea-wrap">
                <label>Your requirement <span class="ex-required ">*</span></label>
                <textarea class="ex-input ex-textarea" placeholder="Brief us your requirement"></textarea>
                <div class="ex-actions">
                  <button class="ex-button" type="submit">Send Message</button>
                </div>
                <div class="ex-richtext">
                  <p>We respect your privacy. We promise we won't spam you :)</p>
                </div>
              </div>
            </div>
          </form>
      </div>
  
    </div>
    </div>
  </div>
</section>




  
</div>
<?php include './common/footer.php' ?>
