<?php include './common/header.php' ?>
<div class="body-container maintenance-page">
  <div class="banner-area">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-6 text-md-right">
          <img src="./images/maintainance/banner-image.svg" alt="Maintenance">
        </div>
        <div class="col-lg-6 order-md-first mt-4 mt-md-0">
          <h2 class="banner-title">Maintenance</h2>
        </div>
      </div>
    </div>
  </div>


<section class="row-1">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5 order-md-last">
      <img src="./images/maintainance/website-maintenance.svg" alt="Website maintenance">
    </div>
    <div class="col-md-6">
      <h2>Website maintenance</h2>
      <p>We are always at your disposal when you need
        us to improve your pages' loading speed or
        repair any technical glitches on your site.
        Our highly qualified development team
        recognizes that it is important for the
        growth of your company and a seamless
        customer experience to keep your website
        up to date. Therefore, we make sure your
        website receives more visitors and generates
        higher income for your company with our
        website maintenance services. </p>
    </div>
  </div>
</div>
</section>

<section class="row-2">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5">
      <img src="./images/maintainance/app-maintenance.svg" alt="Mobile & Web App maintenance">
    </div>
    <div class="col-md-6 align-right">
      <h2>Mobile & Web App maintenance</h2>
      <p>Are you facing app crashes? Don’t worry about them
          anymore. To solve your app issues and ensure
          they never happen again, our highly qualified experts
          take time and research to solve them. With our mobile
          and web app maintenance, we help you fix all sorts of
          current bugs or errors to provide you with smooth
          and glitch-free running apps.</p>
    </div>
  </div>
</div>
</section>

<section class="row-3">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5 order-md-last">
      <img src="./images/maintainance/testing.svg" alt="Testing">
    </div>
    <div class="col-md-6">
      <h2>Testing</h2>
      <p>We have an exclusive set of professional testers
      to test your website and apps to make smooth
      running sites and apps by performing manual
      and automation testing. Our experts ensure
      that the app or website is checked against 
      industry norms. </p> 
      <ul>
        <li>Manual Testing</li>
        <li>Automation Testing</li>
    </ul>
    </div>
  </div>
</div>
</section>

<section class="row-4">
<div class="container">
  <div class="row align-items-center">
    <div class="col-md-5">
      <img src="./images/maintainance/bug-fixing.svg" alt="Bug Fixing">
    </div>
    <div class="col-md-6 align-right">
      <h2>Bug Fixing</h2>
      <p>Are you facing app crashes? Don’t worry about them
        anymore. To solve your app issues and ensure
        they never happen again, our highly qualified experts
        take time and research to solve them. With our mobile
        and web app maintenance, we help you fix all sorts of
        current bugs or errors to provide you with smooth
        and glitch-free running apps. </p>
    </div>
  </div>
</div>
</section>


</div>
<?php include './common/footer.php' ?>
